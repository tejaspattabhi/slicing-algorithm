#include <stdio.h>
#include <string.h> 
#include <stdlib.h>
#include <ctype.h>
#include <math.h>

#define SIZE 50

#define VERTICAL 1
#define HORIZONTAL 0

#define PROCESSED 1
#define UNPROCESSED 0

#define METHOD_ONE 1 
#define METHOD_TWO 2 
#define METHOD_THREE 3

#define SWAPPED 1
#define UNSWAPPED 0

#define VALID 1
#define INVALID 0

#define toDigit(c) (c - '0')

#define DO_NOT_USE_PREVIOUS 0
#define USE_PREVIOUS 1


struct Module
{
	int width;
	int height;
	int mID;
	int index;
	char outMID [SIZE];
};

struct Node 
{
	int flag;
	char thisVar [SIZE / 10];
	struct Module output [SIZE / 4];
	int length;
	
	struct Node * link;
};

int readFile (char *, struct Module *);
int sizingVertical (struct Module *, struct Module *, struct Module *, int, int);
int sizingHorizontal (struct Module *, struct Module *, struct Module *, int, int);
void sortedList (struct Module *, int, int);
int maximum (int, int);
int isPresent (int, int, struct Module *);
int extractModule (int, struct Module *, int, struct Module *);
struct Node * getNode ();
struct Node * addNode (struct Node *, char *);
struct Node * processPolishExpression (struct Node *, struct Module *, int);
void genPolishExpr (char *, char *);
int getRandomMethod ();
void schuffleMethodOne (char *, char *);
void schuffleMethodTwo (char *, char *);
void schuffleMethodThree (char *, char *);
int validExpression (char *);
void generateInitialPolish (int, char *);
int isOperandPresent(int, char *);

int isOperandPresent(int operand,char *polish)
{
	int i ;
	int len = strlen(polish);//length of the polish expression
	int isMatch;
	for(i=0;i<len;i++)
	{   
		isMatch = toDigit(polish[i]);//polish[i] - '0';
		if(isMatch == operand)
		break;
	}
	if(i == len)
	return(0);
	else
	return(1);
}

void generateInitialPolish (int modules, char * output)
{
	int terms;
	time_t t;
	terms = (2*modules)-1;
	int i,j,k,random;
	char polish[terms + 10];
	int operatorAtEnd;

	int flagV = 1;
	srand(time(NULL));
	
	polish[0] = (char)(((int)'0')+(rand()%modules));
	polish[1] = (char)(((int)'0')+(rand()%modules));
	while(polish[0] == polish[1])
	{
		polish[1] = (char)(((int)'0')+(rand()%modules));
	}
	srand((unsigned) time(&t));
	operatorAtEnd = rand()%2;
	if(operatorAtEnd==1)
	polish[terms-1] = 'V';
	else
	polish[terms-1] = 'H';


	for(k=2;k<terms-1;k=k+2)
	{
		if(flagV){
		polish[k] ='V';
		flagV=0;}
		else{
		polish[k] ='H';
		flagV=1;
		}	
	}
	for(j=3;j<terms-1;j=j+2)
	{
		do
		{
		random = (rand()%modules);
		}while(isOperandPresent(random,polish));
		polish[j] = (char)(((int)'0')+random);
	}
	polish [terms] = '\0';
	printf("Generated initial polish expression: %s\n", polish);
	strcpy (output, polish);
}

int validExpression (char * polishExpression)
{
	int i, k, j;
	int countZero, countOne;
	int length;

	i = 0;
	length = strlen (polishExpression);
	
	while (polishExpression[i] != '\0')
	{
		if (isalpha (polishExpression[i])) {
			if ((polishExpression[i] == 'H' && polishExpression[i+1] == 'H') || (polishExpression[i] == 'V' && polishExpression[i+1] == 'V'))
				break;
		}
		i++;
	}
	if (i < length) {
		printf("repeated occurences of H or V ");
		return INVALID;
	}
		
	if ((isdigit (polishExpression [0])) && (isdigit (polishExpression [1])) && ((polishExpression [length - 1] == 'H') || (polishExpression [length - 1] == 'V'))) {
		for(k = 1; k < length; k++) {  	
			countZero = 0;
			countOne = 0;
			
			for(j = 0; j <= k; j++)	{
				if (isdigit (polishExpression[j]))
					countOne++;
				else
					countZero++;
			}
			
			if(countZero > countOne)
				break;			
		}
		if(k < length) {
			printf ("the polish expression does not satisfy blotting property\n");
			return INVALID;
		}
		else {
			printf ("valid\n");
			return VALID;
		}	
	}	
	else {
		printf ("polish expression does not start or end with approriate terms\n");
		return INVALID;
	}
}


void schuffleMethodOne (char * input, char * output) {
	// Swap adjucent operands
	int pos, prevPos;
	char buffer [SIZE];
	char temp;
	int isDone;
	
	pos = -1;
	prevPos = -2;
	isDone = UNSWAPPED;
	strcpy (buffer, input);
	srand(time (NULL));
	
	while (isDone != SWAPPED) {
		while (!isdigit (buffer [pos = (rand () % strlen (input))]));
		if (prevPos == pos)
			continue;
			
		printf ("Test: %c\n", buffer [pos]);
		
		// check if the left side is an integer too
		if (pos != 0) {
			if (isdigit (buffer [pos - 1])) {
				temp = buffer [pos - 1];
				buffer [pos - 1] = buffer [pos];
				buffer [pos] = temp;
				isDone = SWAPPED;
				break;
			}
		}
		
		// check if the right side is an integer
		if (pos != strlen (buffer) - 1) {
			if (isdigit (buffer [pos + 1])) {
				temp = buffer [pos + 1];
				buffer [pos + 1] = buffer [pos];
				buffer [pos] = temp;
				isDone = SWAPPED;
				break;
			}
		}
		prevPos = pos;
	}
	
	strcpy (output, buffer);
}

void schuffleMethodTwo (char * input, char * output) {
	// Complement a substring of operators
	int i;
	int min, max, temp;
	strcpy (output, input);
	srand(time (NULL));
	
	while (!strcmp (output, input)) {
		min = rand () % strlen (input);
		while ((max = rand () % strlen (input)) == min);
			
		printf ("Min: %d\tMax: %d\tInput: %s\tOutput: %s\n", min, max, input, output);
		
		if (min > max) {
			temp = max;
			max = min;
			min = temp;
		}
		
		for (i = min; i < max; i++) {
			if (output[i] == 'H')
				output[i] = 'V';
			else if (output[i] == 'V')
				output[i] = 'H';
		}
	}
}

void schuffleMethodThree (char * input, char * output) {
	// Swap an operand and operator and check for normality
	char buffer [SIZE];
	int pos, isDone;
	int x, y;
	int count = 0;
	
	pos = x = y = -1;
	isDone = UNSWAPPED;
	
	strcpy (buffer, "");
	strcpy (buffer, input);
	pos = rand () % strlen (buffer);
	
	do {
		isDone = UNSWAPPED;
		while (isDone != SWAPPED) {
			if (pos == 0 || (pos == strlen (buffer) - 1)) {
				pos = rand () % strlen (buffer);
			}
			else {
				if (isdigit (buffer [pos]) && isalpha (buffer [pos + 1])) {
					x = pos;
					y = pos + 1;
					isDone = SWAPPED;
				}
				else if (isalpha (buffer [pos]) && isdigit (buffer [pos + 1])) {
					x = pos;
					y = pos + 1;
					isDone = SWAPPED;
				}
				else if (isdigit (buffer [pos]) && isalpha (buffer [pos - 1])) {
					x = pos;
					y = pos - 1;
					isDone = SWAPPED;
				}
				else if (isalpha (buffer [pos]) && isdigit (buffer [pos - 1])) {
					x = pos;
					y = pos - 1;
					isDone = SWAPPED;
				}
				else
					pos = rand () % strlen (buffer);
			}
		}
		
		char temp = buffer [x];
		buffer [x] = buffer [y];
		buffer [y] = temp;
		
		count++;
		
		// check for normality
		// Construct Binary tree
	} while (validExpression (buffer) != VALID && count < 10);
	
	if (count >= 10) {
		strcpy (buffer, input);
		schuffleMethodOne (buffer, output);
	}
	else {	
		strcpy (output, buffer);
	}
}

int getRandomMethod () {
	int method;
	while ((method = rand () % 4) == 0);
	return method;
}

void genPolishExpr (char * input, char * output) {
	int method;
	
	method = -1;
	srand(time (NULL));
	
	method = getRandomMethod ();
	// method = METHOD_ONE;
	// method = METHOD_TWO;
	// method = METHOD_THREE;
	
	if (method == METHOD_ONE)
		schuffleMethodOne (input, output);
	else if (method == METHOD_TWO)
		schuffleMethodTwo (input, output);
	else
		schuffleMethodThree (input, output);
	
}

struct Node * getNode () {
	struct Node * nPtr;
	
	nPtr = (struct Node *) malloc (sizeof (struct Node));
	return nPtr;
}

struct Node * addNode (struct Node * first, char * buffer) {
	struct Node * last, * temp;
	int i;
	
	first = NULL;
	last = NULL;
	temp = NULL;
	
	// strrev (buffer);
	for (i = 0; i < strlen (buffer); i++) {
		temp = getNode ();
		
		temp->thisVar[0] = buffer [i];
		temp->thisVar[1] = '\0';
		temp->flag = UNPROCESSED;
		temp->length = 0;
		
		temp->link = NULL;
		
		if (first == NULL)
			first = temp;
		
		if (last == NULL)
			last = temp;
		else {
			last->link = temp;
			last = temp;
		}
	}
	return first;
}

int readFile (char * fName, struct Module * dimensions) {
	FILE * fPtr;
	int i, module;
	char buffer [SIZE];
	char value [SIZE];
	char * str;
	
	int index;
	int height, width;
	int dCount = 0;
	
	fPtr = fopen (fName, "r");
	module = -1;
	index = -1;
	
	while (fscanf (fPtr, "%s", buffer) != EOF) {
		if (!strcmp (buffer, "#") || buffer[0] == '#') {
			fgets (buffer, SIZE, fPtr);
		}
		else {
			strcpy (value, buffer);
			str = strtok (buffer, ":");
			
			if (!strcmp (value, str)) { 
				//If it is the same then it is a <pair>
				sscanf (value, "<%d", &width);
				
				fscanf (fPtr, "%s", buffer);
				sscanf (buffer, "%d>", &height);
				
				dimensions[dCount].width = width;
				dimensions[dCount].height = height;
				dimensions[dCount].mID = module;
				dimensions[dCount].index = ++index;
				dCount++;
			}
			else {
				module = atoi (str);
				index = 0;
			}
		}
	}
	
	fclose (fPtr);
	// for (i = 0; i < dCount; i++)
		// printf ("Module %d: (%d, %d)\n", dimensions[i].mID, dimensions[i].width, dimensions[i].height);
	
	return dCount;
}

int extractModule (int moduleID, struct Module * dimensions, int dCount, struct Module * moduleList) {
	int i;
	int mCount = 0;
	
	for (i = 0; i < dCount; i++) {
		if (dimensions[i].mID == moduleID)
			moduleList [mCount++] = dimensions [i];
	}
	for (i = 0; i < mCount; i++) {
		sprintf (moduleList [i].outMID, "%d%d", moduleList [i].mID, moduleList [i].index);
	}
	return mCount;
}

int maximum (int a, int b)
{
	if(a >= b)
		return a;
	else
		return b;
}

int isPresent(int a, int b, struct Module *dimensionList)
{
	int i;
	for (i = 0; i < SIZE; i++)
	{
		if (dimensionList[i].height < 0 || dimensionList[i].width < 0)
			return 0;
		else if ((dimensionList[i].width == a) && (dimensionList[i].height ==b))
			return 1;
	}
	
	// SIZE reached
	return 0;
}

int sizingVertical (struct Module * output, struct Module * list1, struct Module * list2, int s, int t)
{
	int i, j, k;
	int width, height;
	
	printf ("Sorting List 1:\n");
	sortedList (list1, s, VERTICAL);
	for (i = 0; i < s; i++) {
		printf ("Module %s: (%d, %d)\n", list1[i].outMID, list1[i].width, list1[i].height);
	}
	printf ("\n");
	
	printf ("Sorting List 2:\n");
	sortedList (list2, t, VERTICAL);
	for (i = 0; i < t; i++) {
		printf ("Module %s: (%d, %d)\n", list2[i].outMID, list2[i].width, list2[i].height);
	}
	printf ("\n");
	
	printf ("Vertical Sizing:\n");
	for (i = 0; i < SIZE; i++)
	{
		output[i].width = -1;
		output[i].height = -1;
		output[i].mID = 0;
		output[i].index = 0;
		strcpy (output[i].outMID, "");
	}

	i = 0;
	j = 0;
	k = 0;
	while ((i < s) && (j < t)) {
		width = list1[i].width + list2[j].width;
		height = maximum (list1[i].height, list2[j].height);
		
		// implementation of union
		if(!isPresent (width, height, output))
		{
			output[k].width = width;
			output[k].height = height;
			sprintf (output[k].outMID, "%s%s", list1[i].outMID, list2[j].outMID); 			
			printf("Pair %s: (width, height) is (%d, %d)\n", output[k].outMID, width, height);
			k++;
		}
		
		if (maximum (list1[i].height, list2[j].height) == list1[i].height)
			i++;
		else	 
			j++;	
	}
	// printf ("\n");
	return k;
}

int sizingHorizontal (struct Module * output, struct Module * list1, struct Module * list2, int s, int t)
{
	int i = 0, j = 0, k = 0;
	int p = 0;
	int width, height;

	printf ("Sorting List 1:\n");
	sortedList (list1, s, HORIZONTAL);
	for (i = 0; i < s; i++) {
		printf ("Module %s: (%d, %d)\n", list1[i].outMID, list1[i].width, list1[i].height);
	}
	printf ("\n");
	
	printf ("Sorting List 2:\n");
	sortedList (list2, t, HORIZONTAL);
	for (i = 0; i < t; i++) {
		printf ("Module %s: (%d, %d)\n", list2[i].outMID, list2[i].width, list2[i].height);
	}
	printf ("\n");
	
	printf ("Horizontal Sizing:\n");
	for (i = 0; i < SIZE; i++)
	{
		output[i].width = -1;
		output[i].height = -1;
		output[i].mID = 0;
		output[i].index = 0;
		strcpy (output[i].outMID, "");
	}

	i = 0;
	while ((i < s) && (j < t))
	{
		height = list1[i].height + list2[j].height;
		width = maximum (list1[i].width, list2[j].width);
		
		// implementation of union
		if(!isPresent (width, height, output))
		{
			output[k].width = width;
			output[k].height = height;
			sprintf (output[k].outMID, "%s%s", list1[i].outMID, list2[j].outMID); 
			printf("Pair %s: (width, height) is (%d, %d)\n", output[k].outMID, width, height);
			k++;
		}
		
		if (maximum (list1[i].width, list2[j].width) == list1[i].width)
			i++;
		else	 
			j++;	
	}
	return k;
}

void sortedList (struct Module * dimensionList, int size, int cut)
{ 
	int i,j,k;
	struct Module swap;
	
	if (cut == VERTICAL) 
	{
		// width should be increasing
		// height should be decreasing
		for (i = 0 ; i < (size - 1); i++)
		{
			for (j = 0 ; j < (size - i - 1); j++)
			{
				if (dimensionList[j].width > dimensionList[j+1].width) 
				{
					swap = dimensionList[j];
					dimensionList[j] = dimensionList[j+1];
					dimensionList[j+1] = swap;   
				}
			}
		}

		// printf("The sorted dimensionList is:\n");
		// for (i = 0; i < size; i++)
			// printf ("(%d, %d)\t", dimensionList[i].width, dimensionList[i].height);
		// printf ("\n\n");
	}
	else // Horizontal cut
	{
		// width should be decreasing
		// height should be increasing
		for (i = 0 ; i < (size - 1); i++)
		{
			for (j = 0 ; j < (size - i - 1); j++)
			{
				if (dimensionList[j].height > dimensionList[j+1].height) /* For decreasing order use < */
				{
					swap = dimensionList[j];
					dimensionList[j] = dimensionList[j+1];
					dimensionList[j+1] = swap;   
				}
			}
		}

		// printf ("The sorted List is:\n");
		// for (i = 0; i < size; i++)
			// printf ("(%d, %d)\t", dimensionList[i].width, dimensionList[i].height);
		// printf ("\n\n");
	}
}

struct Node * processPolishExpression (struct Node * first, struct Module * dimensions, int dCount) {
	struct Node * temp, * prev, * cur, * next;
	int module1, module2;
	
	struct Module m1 [SIZE];
	int lenM1;
	
	struct Module m2 [SIZE];
	int lenM2;
	
	struct Module output [SIZE];
	int outputLen;
	
	int i;
	
	for (i = 0; i < SIZE; i++)	{
		output[i].width = -1;
		output[i].height = -1;
	}
	
	temp = NULL;
	prev = NULL;
	cur = NULL;
	next = NULL;
	
	if (first == NULL)
		return NULL;
	
	next = first->link;
	prev = NULL;
	cur = first;
	while (next != NULL) {
		if (next->thisVar[0] == 'V' || next->thisVar[0] == 'H') {
			printf ("prev: %s\n", prev->thisVar);
			printf ("cur: %s\n", cur->thisVar);
			printf ("next: %s\n\n", next->thisVar);
			
			if (prev->flag == PROCESSED) {
				for (i = 0; i < prev->length; i++)
					m1[i] = prev->output[i];
				lenM1 = prev->length;
			}
			else {
				module1 = atoi (prev->thisVar);
				lenM1 = extractModule (module1, dimensions, dCount, m1);
			}
			
			if (cur->flag == PROCESSED) {
				for (i = 0; i < cur->length; i++)
					m2[i] = cur->output[i];
				lenM2 = cur->length;
			}
			else {
				module2 = atoi (cur->thisVar);
				lenM2 = extractModule (module2, dimensions, dCount, m2);
			}
	
			if (next->thisVar[0] == 'H') {
				outputLen = sizingHorizontal (output, m1, m2, lenM1, lenM2);
			}
			else {
				outputLen = sizingVertical (output, m1, m2, lenM1, lenM2);
			}
			
			// Place it back darling :*
			
			prev->flag = PROCESSED;
			strcpy (prev->thisVar, "");
			printf ("Sized to %d\n", outputLen);
			for (i = 0; i < outputLen; i++) {
				prev->output[i] = output[i];
				printf ("Module %s: (%d, %d)\n", output[i].outMID, output[i].width, output[i].height);
			}
			printf ("\n\n");
			prev->length = outputLen;
			
			prev->link = next->link;
			
			free (cur);
			free (next);
			
			prev = NULL;
			cur = first;
			next = first->link;
		}
		else {
			prev = cur;
			cur = next;
			next = next->link;
		}
	}
	
	return first;
}

int main (int argc, char * argv[])
{
	struct Module mList1 [SIZE];
	int mListCount1;
	
	struct Module mList2 [SIZE];
	int mListCount2;
	
	struct Module dimensions [SIZE];
	int dCount;
	
	char buffer [SIZE];
	int i, maxMID;
	struct Node * first = NULL;
	struct Node * temp;
	
	struct Module bestDimen;
	char bestPolish [SIZE], curPolish [SIZE];
	float bestArea, curArea;
	int temperature;
	
	FILE * fptr;
	
	bestArea = 1000000;

	if (argc != 2) {
		printf ("Usage: %s <filename>\n", argv[0]);
		return -1;
	}

	dCount = readFile (argv[1], dimensions);
	
	maxMID = -1;
	for (i = 0; i < dCount; i++) {
		printf ("Module %d[%d]: (%d, %d)\n", dimensions[i].mID, dimensions[i].index, dimensions[i].width, dimensions[i].height);
		if (dimensions[i].mID > maxMID)
			maxMID = dimensions[i].mID;
	}
	printf ("\n\n");
	
	temperature = 100; // Farenhiet
	int limit = 0.85 * temperature;
	
	fptr = fopen ("SIMULATED_ANNEALING.txt", "w");
	
	// Assuming MaxMID starts from 0
	generateInitialPolish (maxMID + 1, buffer);
	strcpy (bestPolish, buffer);
	first = addNode (first, buffer);
	// first = addNode (first, "12V");
	fprintf (fptr, "Polish Expression: %s\n\n", buffer);
	
	first = processPolishExpression (first, dimensions, dCount);
	temp = first;
	printf ("Output: \n");
	while (temp != NULL) {
		for (i = 0; i < temp->length; i++) {
			printf ("Module %s: (%d, %d)\n", temp->output[i].outMID, temp->output[i].width, temp->output[i].height);
			fprintf (fptr, "Module %s: (%d, %d)\n", temp->output[i].outMID, temp->output[i].width, temp->output[i].height);
			if (temp->output[i].width * temp->output[i].height <= bestArea) {
				bestArea = (float) (temp->output[i].width * temp->output[i].height);
				bestDimen = temp->output[i];
			}
		}
		temp = temp->link;
	}
	
	fprintf (fptr, "\nModule Causing Minimum Area: %s, Minimum Area: %f, Dimensions (%d, %d)\n", bestDimen.outMID, bestArea, bestDimen.width, bestDimen.height);
	free (first);
	first = NULL;
	
	curArea = bestArea;
	strcpy (curPolish, bestPolish);
	
	float tempArea;
	struct Module tempDimen;
	float deltaArea;
	int flag = DO_NOT_USE_PREVIOUS;
	
	for (i = 0; i < limit; i++) {
		if (flag == DO_NOT_USE_PREVIOUS) {
			genPolishExpr (curPolish, buffer);
			strcpy (curPolish, buffer);
		}
		else
			flag = DO_NOT_USE_PREVIOUS;
		
		printf ("Polish Expression: %s\n", curPolish);
		fprintf (fptr, "Polish Expression: %s\n", curPolish);
		
		first = addNode (first, curPolish);
		first = processPolishExpression (first, dimensions, dCount);
		temp = first;
		tempArea = bestArea;
		
		while (temp != NULL) {
			for (i = 0; i < temp->length; i++) {
				// printf ("Module %s: (%d, %d)\n", temp->output[i].outMID, temp->output[i].width, temp->output[i].height);
				if (temp->output[i].width * temp->output[i].height <= tempArea) {
					fprintf (fptr, "Module %s: (%d, %d)\n", temp->output[i].outMID, temp->output[i].width, temp->output[i].height);
					tempArea = temp->output[i].width * temp->output[i].height;
					tempDimen = temp->output[i];
				}
			}
			temp = temp->link;
		}
		
		fprintf (fptr, "\nModule Causing Minimum Area: %s, Minimum Area: %f, Dimensions (%d, %d)\n", tempDimen.outMID, tempArea, tempDimen.width, tempDimen.height); 
		printf ("\nModule Causing Minimum Area: %s, Minimum Area: %f, Dimensions (%d, %d)\n", tempDimen.outMID, tempArea, tempDimen.width, tempDimen.height); 
		free (first);
		first = NULL;
		
		deltaArea = tempArea - bestArea;
		
		if (deltaArea < 0.0) {
			strcpy (bestPolish, curPolish);
			bestArea = tempArea;
			bestDimen = tempDimen;
			// flag = USE_PREVIOUS;
			// i = 0;
			printf ("Polish Expression Causing Minimum Area. Re-pertubating.."); 
			fprintf (fptr, "Polish Expression Causing Minimum Area. Re-pertubating...."); 
		}
		else {
			float x = (float) exp (-1 * deltaArea / temperature);
			if (x > 0.7) {
				strcpy (bestPolish, curPolish);
				bestArea = tempArea;
				bestDimen = tempDimen;
				// flag = USE_PREVIOUS;
				// i = 0;
				printf ("Polish Expression probability > 0.7. Re-pertubating.."); 
				fprintf (fptr, "Polish Expression probability > 0.7. Re-pertubating.."); 
			}
		}
	}
	
	fclose (fptr);
	
	

	return 0;
}