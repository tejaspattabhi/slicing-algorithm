###########################################################################################
Name: Naga Venkata Anjaneya Prasad Suryadevara
Email: nsbd3@mst.edu
Project: Algorithm to produce minimum area floor plan by using Simulated Annealing.
###########################################################################################
 Please follow these instructions:
 
 1. simulated_annealing.c is the file to be executed which takes an design file as an input.
 2. In the design file Module ID's start from 0 to 9.
 3. The input file should should name the modules starting from 0 instead of 1.
 4. Module representation format in the output generated
	XY
	X denotes the module number
	Y denotes the shape number in the current module
	Eg
	0: <2 7> <3 5> <4 4> <5 3> <7 2> <10 1> <1 10> <2 9> <9 2>
		<3 5>  will be represented as 02
	
5. 021331 in the output represents the block floor plan consisting of 
	shape 2 of module 0
	shape 3 of module 1
	shape 1 of module 3
	
6. SIMULATED_ANNEALING will have an account of all intermediate steps executed.

Note:
- Every single module working soundly. The Final algorithm running on an infinite loop (almost)
- Please make sure that the MAX Nodes are 10 [STARTING from 0 and LAST 9]

